<?php
session_start();
	if($_POST["do"] = "start") {
		if (!empty($_POST["player-name"])) {
			$playerName = mysql_escape_string($_POST["player-name"]);
			require_once("config.php");
			$url = $CONFIG["backendURL"] . "games/" . $playerName;
			 
			// send a request for a new game to the backend
			$ch = curl_init($url);			 
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			curl_close($ch);

			$gameInfo = json_decode($response, true);		

			// error case: check if the request failed
			if ($gameInfo == NULL) {
				echo '<span class="error">Error: Didn\'t get game data from the backend. Please make sure it is running correctly.</span>';
			}
		}
	}
?>

<h1>Start a new Game</h1>
<form name="form-data" id="form-data" action="index.php?do=start" method="POST">
	<!-- name input field, non alphanumeric chars are filtered by js -->
	<input id="player-name" name="player-name" class="lock-x" placeholder="Your Name (only alphanumeric characters)" onkeyup="this.value=this.value.replace(/[^(A-Z|a-z|0-9)]/g,'');" type="text"/><br /><br />
	<input value="Let's goo!" type="submit" />
</form>

<?php
	if (!isset($gameInfo["game_created"]) || !isset($gameInfo["id"])) {
		break;
	}
	$created = $gameInfo["game_created"];
	if($created) {
		// start the game and set initial session values
		$_SESSION["playerId"] = $gameInfo["id"]; 
		$_SESSION["playerName"] = $playerName;
		$_SESSION["playerPoints"] = intval($CONFIG["startPoints"]);
		$_SESSION["playerRound"] = 0;
		$_SESSION["playerAttitudePoints"] = 0;

		// redirect to the game
		echo '<form method="POST" action="index.php?do=game" id="game-data" name="game-data"><input name="do" type="hidden" value="game" /> </form>';
		echo '<script>$(document).ready(function(){$("#game-data").submit();});</script>';
	}	
	else {
		// error 
		echo '<span class="error">An error occured. Maybe, too many players are playing the game currently. Please try again later.</span>';
	}
?>