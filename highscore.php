<section class="highscore">
<h1>Game Highscore</h1>

<?php
	// send a GET request to get the highscore (JSON)
	function getHighscore()
	{
		require_once("config.php");
		$url = $CONFIG["backendURL"] . "highscores.json";

		$ch = curl_init($url);	 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);

		return json_decode($response, true);		
	}


	// builds the formated output (html table) from the highscore array
	// $type: should be "top" or "worst" 
	function buildOutput($data, $type) {											
		// top players
		$output .= "<table><tr><th>Position</th><th>Points</th><th>Player Name</th></tr>";
		for ($pos = 1 ; $pos < 6 ; $pos++) {
			$points = $type . $pos;			
			$playerName = $points . "name";
			$output .= "<tr><td class='pos'>" . $pos . "</td><td class='points'>" . 
									$data[$points] . "</td><td class='playerName'>" . $data[$playerName] . "</td></tr>";
		}
		$output .= "</table>";	
		
		return $output;
	}
			
	$highscore = getHighscore();
	$html = "<h2>Top good players</h2>" . buildOutput($highscore[0], "top") . "<h2>Top evil players</h2>" . buildOutput($highscore[0], "worst");;
	echo $html;
?>

<span class="backlink">[<a href="index.php">back to menu</a>]</span>
</section>
