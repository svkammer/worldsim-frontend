<?php
	session_start();
	require_once("config.php");

	/*
		Returns the user and world status image filenames and the user status description based on the current attribute points of the user.
		The data is returned in an array: $status["user"], $status["earth"] and $status["text"]
	*/
	function getCurrentStatus($points) {

		// get user and world status
		if($points >= 4500 ) {
			$userStatus = "greater_force";
			$earthStatus = "status1";
			$userText = '<span class="user_great">Greater Force</span>';
		}
		else if($points >= 2500 ) {
			$userStatus = "angel";
			$earthStatus = "status2";
			$userText = '<span class="user_great">Angel</span>';
		} 
		else if($points >= 1000 ) {
			$userStatus = "philantroph";
			$earthStatus = "status3";
			$userText = '<span class="user_good">Philantroph</span>';
		} 
		else if($points >= -500 ) {
			$userStatus = "neutral";
			$earthStatus = "status4";
			$userText = '<span class="user_neutral">Neutral</span>';
		}
		else if($points >= -1500 )  {
			$userStatus = "misanthrop";
			$earthStatus = "status5";
			$userText = '<span class="user_bad">Misanthrop</span>';
		}
		else if($points >= -7000 ) {
			$userStatus = "daemon";
			$earthStatus = "status6";
			$userText = '<span class="user_worse">Daemon</span>';
		}
		else {
			$userStatus = "devil";
			$earthStatus = "status7";
			$userText = '<span class="user_aweful">Devil</span>';
		}

		$userStatus .= ".png";
		$earthStatus .= ".png";

		$status = array("user" => $userStatus, "earth" => $earthStatus, "text" => $userText);
		return $status;
	}

	/*
		Uses the $options parameter (json decoded array from the backend) to generate a html table with the available options in a round.
		Returns the html output.
	*/
	function formatOptions($options) {
		$count = 0;
		$output .= "<table class='optionsTable' cellspacing='10'><tr>";
		foreach ($options["options"] as &$option) {
			$output .= "<td id='".$option["id"]."' attrpoints='".$option["attitude_points"]."' epoints='".$option["earn_points"]."''><b>" . $option["name"] . "</b><br />";
			if ($option["description"] != NULL)
				$output .= $option["description"];
			
			$output .= "<br/>This action costs <img src='images/cost.png' /> " . $option["points"] . " points.</td>";
			
			$count++;
			// display two options in each row
			if ($count % 2 == 0) {
				$output .= "</tr>";
				$output .= "<tr>";
			}
		}
		$output .= "</table>";
		return $output;
	}

	/*
		makes an request to the backend to get the 
	*/
	function getRoundOptions($playerId, $points, $backendURL) {
		// get options
		$url = $backendURL . "options/" . $playerId . "/" . $points . ".json";
				 
		// send a request game options
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);

		// error cases
		if($response == NULL || empty($playerId)) {
			die('<div class="error">Oh noes! :( Your session is expired or your session data is wrong, please return to <a href="index.php">menu</a> to restart the game.</div>');
		}

		return json_decode($response, true);
	}

	if (isset($_POST["updatePoints"])) {
		$_SESSION["playerPoints"] = $_SESSION["playerPoints"] + intval($_POST["updatePoints"]);
		$_SESSION["playerAttitudePoints"] = $_SESSION["playerAttitudePoints"] + intval($_POST["attrpoints"]);
	}
 
	// get player data from session
	$playerId = intval($_SESSION["playerId"]);
	$playerName = $_SESSION["playerName"];
	$playerPoints = $_SESSION["playerPoints"];
	$_SESSION["playerRound"] = $_SESSION["playerRound"] + 1;
	$playerRound = $_SESSION["playerRound"] > $CONFIG["numberOfRounds"] ? $CONFIG["numberOfRounds"] : $_SESSION["playerRound"];

	// end of game
	if ($_SESSION["playerRound"] > $CONFIG["numberOfRounds"]) {
		 
		 	$url = $CONFIG["backendURL"] . "games/" . $playerId . "/" .  $_SESSION["playerAttitudePoints"];
			
			// send PUT request to the backend to add the user score to the highscore
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_PUT, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_exec($ch);
			curl_close($ch);

			// game finished message
			$endMessage = "<h1>Game finished</h1>";
			$endMessage .= "You played " . $CONFIG["numberOfRounds"] . " rounds.<br />";
			if($_SESSION["playerAttitudePoints"] > 0) {
				$endMessage .= "You were a <b>gentle</b> user and earned <b>" . $_SESSION["playerAttitudePoints"] . " attitude points</b>!<br />";
			}
			else {
				$endMessage .= "You were a <b>naughty</b> user and got <b>" . $_SESSION["playerAttitudePoints"] . " attitude points</b>!<br />";
			}
			$endMessage .= "You also have <img src='images/cost.png' /> <b>" . $_SESSION["playerPoints"] . " credit points</b> at the end of this game.<br />";
			echo $endMessage;

	  		session_destroy(); 
	}

	$options = getRoundOptions($playerId, $_SESSION["playerPoints"], $CONFIG["backendURL"]);
	$output = formatOptions($options);
	$status = getCurrentStatus($_SESSION["playerAttitudePoints"]);
?>

<form id="game-info" name="game-info" method="POST">
	<center><table class="statusTable" cellspacing="20">
		<td><b>Round</b><br /><span class="roundInfo"><?=$playerRound?></span></td>
		<td><b>User Status: <?=$status["text"]?></b></br><img src="images/user/<?=$status["user"]?>" />  </td>
		<td> <b>World Status</b><br/><img src="images/earth/<?=$status["earth"]?>" height="128" /></td>
		<td><b>Credits</b><br /><span style="line-height: 128px;"><img style="vertical-align: middle;" src='images/costBig.png' /> <span class="pointInfo"><?=$playerPoints?></span></span></td>
	</table></center>

	<?php 
	if($_SESSION["playerRound"] != $CONFIG["numberOfRounds"] + 1)
		echo "<span class='optionInfo'>Hey <b>$playerName</b>, Please choose an option:</span>";
	else {
		// end-of-the-game - buttons
		echo "<br /><br /><input id='highscoreButton' value='See if you made it into Highscore' type='submit' />";
		echo "<br /><input id='againButton' value='Have fun again! :)' type='submit' />";
	}
	?>
	
	<section class="gameOptions">
		<?=$output;?>
	</section>

	<div class="footer">WorldSim v0.2 &#183; <a href="index.php?do=about" target="_blank">about</a> &#183; <a href="index.php?do=menu">back to game menu</a></div>	
</form>


<!-- Jquery scripts to make buttons clickable -->
<script>
	$(document).ready(function() {
		$(".optionsTable td").mouseover(function() {
			$(this).css("background-color", "orange");
		});
		$(".optionsTable td").mouseout(function() {
			$(this).css("background-color", "#ccc");
		});
	$(".optionsTable td").click(function() {
  		
  		// send score and got to next round
		$.ajax({
		  url: "game.php?do=game",
		  data: "updatePoints=" + $(this).attr("epoints") + "&attrpoints=" + $(this).attr("attrpoints"),
	      type: "POST"
		});
  		$("#game-info").submit();
	});

	$("#againButton").click(function() {
		window.location.replace("index.php?do=start");
		return false;
	});
	$("#highscoreButton").click(function() {
		window.location.replace("index.php?do=highscore");
		return false;
	});

	});
</script>
