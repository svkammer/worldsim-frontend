<?php
	if(!file_exists("config.php")) {
		die("Config file is missing! Please run setup_config.php first.");
	}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>WorldSim</title>

    <!-- Meta data -->
    <meta name="description" content="A startpage to navigate quickly in the net.">
    <meta name="author" content="Sven Kammerer / Christian Lange">
    
    <!-- Jquery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Coda' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Source+Code+Pro' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Englebert' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="css/style.css">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>

  <body>
  	<h1 class="header">
      <span style="color: grey; font-size: 15px">[cozy]</span> World<i style="color: darkgreen">Sim</i>
    </h1>
    
    <!-- Make header clickable -->
    <script>
      $(document).ready(function(){
        $(".header").click(function() {
          window.location.replace("index.php");
        });
      });
    </script>

    <?php
      // embed the correct page based on the $_GET['do'] parameter
      if ($_GET["do"] == "start") {
    		require_once("start_game.php");
      }
      else if ($_GET["do"] == "highscore") {
    		require_once("highscore.php");
      }
      else if ($_GET["do"] == "about") {
     		require_once("about.php");
      }
      else if ($_GET["do"] == "game") {
        require_once("game.php");
      }
      else {
      	require_once("menu.php");
      }
    ?>	
  </body>
</html>
