<h1>About the Game</h1>
<div class="about-text">This game was developed by <a target="_blank" href="http://chlange.ganz-sicher.net">Christian Lange</a> and 
<a target="_blank" href="http://ganz-sicher.net">Sven Kammerer</a> during a student project at the university of applied science HTWG Constance.</div>

<span class="about-text">[<a href="index.php">back to menu</a>]</span>
