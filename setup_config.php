<?php
	$configName = "config.php";

	if(file_exists($configName))
		die("Error, please delete the config file " . $configName . " first, before you create a new configuration!");
?>

<link rel="stylesheet" href="css/style.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

<h1>WorldSim FrontEnd Installation - Configuration Settings</h1>
<form style="padding: 15px; text-align: left !important; margin-left: 15px;" action="setup_config.php" method="POST">
	Backend Address: <input class="lock-x" name="backendUrl" value="http://localhost:3000/" type="text" /><br />
	Credit points at the beginning of the game: <input class="lock-x" name="startPoints" value="10" type="text" /><br />
	Number of rounds per Game: <input class="lock-x" name="numberOfRounds" value="15" type="text" /><br /><br />
	<input name="do" value="createConfig" type="hidden" />
	<input value="Submit" type="submit" />
</form>

<?php
if ($_POST["do"] == "createConfig") {
	
	$createError = "Can't create file: If you are trying to get this game to run and you are having errors, you might want to check that you have granted your PHP file access to write information to the hard drive. Setting permissions is most often done with the use of an FTP program to execute a command called CHMOD. Use CHMOD to allow the PHP file to write to disk, thus allowing it to create a file.";
	$f = fopen($configName, 'w') or die($createError);
 	$content = 
 	'<?php
 		$CONFIG["backendURL"] = "' . $_POST["backendUrl"] . '";
 		$CONFIG["startPoints"] = ' . $_POST["startPoints"] . ';
 		$CONFIG["numberOfRounds"] = ' . $_POST["numberOfRounds"] . ';
 	?>';
 	fwrite($f, $content);
	fclose($f);

	if(file_exists($configName)) {
		// success message
		die("Success! :) Your configuration was written to your " . $configName . ".");
	}
	else {
		// error case
		die("An error occured: Could not write config file. Please try again.");
	}
}
?>